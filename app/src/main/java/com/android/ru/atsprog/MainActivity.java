package com.android.ru.atsprog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Selection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView lvMain;
    ListView lvList;
    final int DIALOG_EXIT = 1;
    String Number = "899191919 ";
    String Login =" " ;
    String Password= " ";
    String LineTel =" ";
    String smsPull  = " ";
    String Url  = " ";
    String value = " ";
    Button btnSearch;
    SearchView searchView;

    SharedPreferences sPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SearchView searchView = (SearchView) findViewById(R.id.search) ;
        lvList = (ListView) findViewById(R.id.lvMain);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (null!=searchManager ) {
            assert searchView != null;
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        assert searchView != null;
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                                              @Override
                                              public boolean onQueryTextSubmit(String query) {

                                                  return false;
                                              }

                                              @Override
                                              public boolean onQueryTextChange(String newText){

                                                  Uri uri = ContactsContract.Contacts.CONTENT_URI;
                                                  String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts._ID};
                                                  String selection = ContactsContract.Contacts.DISPLAY_NAME + " LIKE ?";
                                                  String[] selectionArgs = new String[]{"%" + newText + "%"};

                                                  final Cursor cursor = getContentResolver().query(uri, projection, selection, selectionArgs, ContactsContract.Contacts.DISPLAY_NAME + " ASC");


                                                  final SimpleCursorAdapter adapter = new SimpleCursorAdapter(MainActivity.this,
                                                          android.R.layout.simple_list_item_1,
                                                          cursor,
                                                          new String[]{ContactsContract.Contacts.DISPLAY_NAME},
                                                          new int[]{android.R.id.text1,});
                                                  lvMain.setAdapter(adapter);
                                                  lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                      public void onItemClick(AdapterView<?> parent, View view,
                                                                              final int position, long id) {
                                                          showDialog(DIALOG_EXIT);
                                                          //Log.d(LOG_TAG, "itemClick: position = " + position + ", id = "
                                                          //      + id);
                                                          //final Cursor  = managedQuery(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);(ACCOUNT_TYPE = 'vnd.sec.contact.phone' OR ACCOUNT_TYPE = 'com.htc.android.pcsc' OR ACCOUNT_TYPE = 'com.sonyericsson.localcontacts')
                                                          final Cursor c = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                                                  new String[] {ContactsContract.CommonDataKinds.Phone._ID, ContactsContract.CommonDataKinds.Phone.NUMBER},
                                                                  ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                                                                  new String[] {String.valueOf(id)}, null);

                /*final SimpleCursorAdapter adapter1 = new SimpleCursorAdapter(MainActivity.this,
                        android.R.layout.simple_list_item_1,
                        c,
                        new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                        new int[]{android.R.id.text1},
                0                );*/
                                                          final ArrayList<String> contacts = new ArrayList<String>();
//                do {
                                                          c.moveToFirst();
                                                          for(int i=0; i<c.getCount();i++){
                                                              int j1=0;

                                                              String Number = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                                              for (int j=0; j<contacts.size(); j++) {
                                                                  if (contacts.get(j).compareToIgnoreCase(Number) == 0) {
                                                                      j1=1;
                                                                  }
                                                              }
                                                              if(j1!=1){
                                                                  contacts.add(Number);
                                                              }
                                                              c.moveToNext();
                                                          }

                                                          final ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, contacts);
                                                          AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                                                                  MainActivity.this);
                /*builderSingle.setAdapter(adapter1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });*/
                                                          String title = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                                          //String Phone = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                                          builderSingle.setTitle(title);
                                                          lvList.setAdapter(adapter1);
                                                          builderSingle.setNegativeButton("Cancel",
                                                                  new DialogInterface.OnClickListener() {
                                                                      @Override
                                                                      public void onClick(DialogInterface dialog, int which) {
                                                                          dialog.dismiss();
                                                                      }
                                                                  });
                                                          //Выборка номеров телефона


                                                              lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                                  @Override
                                                                  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                                                      //String phone = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                                                      //String phone = adapter1.getItem(position).toString();
                                                                      String phone = contacts.get(position);
                                                                      //String phone = adapter1.getItemAtPosition(which).toString();
                                                                      Intent intent = new Intent(MainActivity.this, ContentActivity.class);
                                                                      intent.putExtra("Number", Number);
                                                                      intent.putExtra("Login", Login);
                                                                      intent.putExtra("Pass", Password);
                                                                      intent.putExtra("LineTel", LineTel);
                                                                      intent.putExtra("smsPull", smsPull);
                                                                      intent.putExtra("phone", phone);
                                                                      intent.putExtra("Url", Url);
                                                                      startActivity(intent);
                                                                  }
                                                              });

                                                      }
                                                  });
                                                  return false;
                                              }
                                          });

        //загрузка данных из памяти
        sPref = getPreferences(MODE_PRIVATE);
        String savedText = sPref.getString("MyNumber", "");
        String savedText2 = sPref.getString("MyLogin", "");
        String savedText3 = sPref.getString("MyPass", "");
        String savedText4 = sPref.getString("MyLineTel", "");
        String savedText5 = sPref.getString("MySmsPull", "");
        String savedText6 = sPref.getString("Url", "");
        Number = savedText;
        Login = savedText2;
        Password = savedText3;
        LineTel = savedText4;
        smsPull = savedText5;
        Url = savedText6;

        lvMain = (ListView) findViewById(R.id.lvMain);

        final Cursor cursor = managedQuery(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");

        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                cursor,
                new String[]{ContactsContract.Contacts.DISPLAY_NAME},
                new int[]{android.R.id.text1,});
        //Log.d(LOG_TAG, "cursor cursor ");

        // заполняем списoк значениями из адаптера
        lvMain.setAdapter(adapter);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {
                showDialog(DIALOG_EXIT);
                //Log.d(LOG_TAG, "itemClick: position = " + position + ", id = "
                //      + id);
                //final Cursor  = managedQuery(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);(ACCOUNT_TYPE = 'vnd.sec.contact.phone' OR ACCOUNT_TYPE = 'com.htc.android.pcsc' OR ACCOUNT_TYPE = 'com.sonyericsson.localcontacts')
                /*final Cursor c = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                        new String[] {ContactsContract.Data._ID, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Nickname.DISPLAY_NAME},
                        ContactsContract.Data.CONTACT_ID + "=?",
                        new String[] {String.valueOf(id)}, null);*/
                final Cursor c = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        new String[] {ContactsContract.CommonDataKinds.Phone._ID, ContactsContract.CommonDataKinds.Phone.NUMBER},
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                        new String[] {String.valueOf(id)}, null);

                /*final SimpleCursorAdapter adapter1 = new SimpleCursorAdapter(MainActivity.this,
                        android.R.layout.simple_list_item_1,
                        c,
                        new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                        new int[]{android.R.id.text1},
                0                );*/
                final ArrayList<String> contacts1 = new ArrayList<String>();
//                do {
                c.moveToFirst();
                for(int i=0; i<c.getCount();i++){
                    int j1=0;

                    String Number = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    for (int j=0; j<contacts1.size(); j++) {
                        if (contacts1.get(j).compareToIgnoreCase(Number) == 0) {
                            j1=1;
                        }
                    }
                    if(j1!=1){
                        contacts1.add(Number);
                    }
                    c.moveToNext();
                }

                final ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, contacts1);
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                        MainActivity.this);
                /*builderSingle.setAdapter(adapter1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });*/
                String title = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                //String Phone = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                lvMain.setAdapter(adapter1);
                builderSingle.setTitle(title);
                builderSingle.setNegativeButton("ОТМЕНА",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.dialog,
                        (ViewGroup)findViewById(R.id.lvList));

                //Выборка номеров телефона
                builderSingle.setAdapter(); {

                    lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            //String phone = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            //String phone = adapter1.getItem(position).toString();
                            String phone = contacts1.get(position);
                            //String phone = adapter1.getItemAtPosition(which).toString();
                            Intent intent = new Intent(MainActivity.this, ContentActivity.class);
                            intent.putExtra("Number", Number);
                            intent.putExtra("Login", Login);
                            intent.putExtra("Pass", Password);
                            intent.putExtra("LineTel", LineTel);
                            intent.putExtra("smsPull", smsPull);
                            intent.putExtra("phone", phone);
                            intent.putExtra("Url", Url);
                            startActivity(intent);
                        }

                    });
                }
                builderSingle.show();
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    //Нажатие на меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings://значок настроек

                Intent intent = new Intent(this, NameActivity.class);
                intent.putExtra("Number", Number);
                intent.putExtra("Login", Login);
                intent.putExtra("Password", Password);
                intent.putExtra("LineTel", LineTel);
                intent.putExtra("smsPull", smsPull);
                intent.putExtra("Url", Url);
                startActivityForResult(intent, 1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //Срабатывае после заполнения окна настроек
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null){
            return;}
        final String MyNumber = data.getStringExtra("Number");
        Number = MyNumber;
        final String MyLogin = data.getStringExtra("Login");
        Login = MyLogin;
        String MyPassword = data.getStringExtra("Password");
        Password = MyPassword;
        String MyLineTel = data.getStringExtra("LineTel");
        LineTel = MyLineTel;
        String MysmsPull = data.getStringExtra("smsPull");
        smsPull = MysmsPull;
        String MyUrl = data.getStringExtra("Url");
        Url = MyUrl;
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("MyNumber", Number);
        ed.putString("MyLogin", Login);
        ed.putString("MyPass", Password);
        ed.putString("MyLineTel", LineTel);
        ed.putString("MySmsPull", smsPull);
        ed.putString("Url", Url);
        ed.commit();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("MyNumber", Number);
        ed.putString("MyLogin", Login);
        ed.putString("MyPass", Password);
        ed.putString("MyLineTel", LineTel);
        ed.putString("MySmsPull", smsPull);
        ed.putString("Url", Url);
        ed.commit();
    }

}

