package com.android.ru.atsprog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NameActivity extends AppCompatActivity implements View.OnClickListener {
    Button button2;
    EditText MyNum;
    EditText MyLogin;
    EditText MyPass;
    EditText MyLine;
    EditText MySmsPull;
    EditText MyUrl;
    String Number = "";
    String Login = "";
    String Password = "";
    String LineTel = "";
    String smsPull = "";
    String Url = "";
    SharedPreferences sPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        /*editText = (EditText) findViewById(R.id.editText);
        editText2 = (EditText) findViewById(R.id.editText2);*/
        button2 = (Button) findViewById(R.id.button2);
        assert button2 != null;
        button2.setOnClickListener(this);
        MyNum = (EditText) findViewById(R.id.editMyNumber);
        MyLogin = (EditText) findViewById(R.id.editLogin);
        MyPass = (EditText) findViewById(R.id.editPass);
        MyLine = (EditText) findViewById(R.id.editLine);
        MySmsPull = (EditText) findViewById(R.id.editSmsPull);
        MyUrl = (EditText) findViewById(R.id.editUrl);

        Intent intent = getIntent();
        Number = intent.getStringExtra("Number");
        Login = intent.getStringExtra("Login");
        Password = intent.getStringExtra("Password");
        LineTel = intent.getStringExtra("LineTel");
        smsPull = intent.getStringExtra("smsPull");
        Url = intent.getStringExtra("Url");

        MyNum.setText(Number);
        MyLogin.setText(Login);
        MyPass.setText(Password);
        MyLine.setText(LineTel);
        MySmsPull.setText(smsPull);
        MyUrl.setText(Url);
    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(this, MainActivity.class);
        Number = MyNum.getText().toString();
        Login = MyLogin.getText().toString();
        Password = MyPass.getText().toString();
        LineTel = MyLine.getText().toString();
        smsPull = MySmsPull.getText().toString();
        Url = MyUrl.getText().toString();


        intent.putExtra("Number", Number);
        intent.putExtra("Login", Login);
        intent.putExtra("Password", Password);
        intent.putExtra("LineTel", LineTel);
        intent.putExtra("smsPull", smsPull);
        intent.putExtra("Url", Url);
        //intent.putExtra("name", editText.getText().toString());
        //intent.putExtra("name2", editText.getText().toString());


        setResult(RESULT_OK, intent);
        finish();
    }

    public void onClickCancel(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        setResult(RESULT_OK, intent);
        finish();
    }
}