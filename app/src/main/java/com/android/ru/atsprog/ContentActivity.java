package com.android.ru.atsprog;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ContentActivity extends AppCompatActivity implements View.OnClickListener {
    TextView contentView;
    TextView txtUri;
    Button button;
    String contentText = null;
    String Number = " ";
    String Login =" " ;
    String Password= " ";
    String LineTel =" ";
    String smsPull  = " ";
    String phone  = " ";

    String Url  = " ";
    Integer leng = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        Intent intent = getIntent();
        Number = intent.getStringExtra("Number");
        Login = intent.getStringExtra("Login");
        Password = intent.getStringExtra("Pass");
        LineTel = intent.getStringExtra("LineTel");
        smsPull = intent.getStringExtra("smsPull");
        Url = intent.getStringExtra("Url");
        phone = intent.getStringExtra("phone");

        txtUri = (TextView) findViewById(R.id.txtUri);
        button = (Button) findViewById(R.id.button);

        contentView = (TextView) findViewById(R.id.content);
        //contentView.setText(phone);
        String txtUrl = Url + "login=" + Login + "&pass=" + Password + "&num_fr=" + Number + "&num_to=" + phone + "&line_c=" + LineTel;
        txtUri.setText(txtUrl);
    }

    public void onClick(View view) {
        if(contentText==null)
            new ProgressTask().execute();
    }

    class ProgressTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... path) {
            String content;
            try{
                content = getContent(Url + "?login=" + Login + "&pass=" + Password + "&num_fr=" + Number + "&num_to=" + phone + "&line_c=" + LineTel);
            }
            catch (IOException ex){
                content = ex.getMessage();
            }
            return content;
        }
        @Override
        protected void onProgressUpdate(Void... items) {
        }
        @Override
        protected void onPostExecute(String content) {
            contentView.setText(phone);
            contentText=content;
            contentView.setText(content.toString());
           //contentView.setText(phone);
        }
        private String getContent(String path) throws IOException {
            BufferedReader reader = null;
            try {
                URL url = new URL(path);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setReadTimeout(10000);
                c.connect();
                reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                StringBuilder buf = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    buf.append(line + "\n");
                }
                return (buf.toString());
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
        }
    }
}

